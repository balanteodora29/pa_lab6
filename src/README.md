# Laboratorul 6

[Link cerinta](https://profs.info.uaic.ro/~acf/java/labs/lab_06.html)

## Compulsory
Create the following components:

1. The main frame of the application.  
2. A configuration panel for introducing parameters regarding the shapes that will be drawn: the size, the number of sides, the stroke, etc.
The panel must be placed at the top part of the frame. The panel must contain at least one label and one input component for specifying the size of the component.
3. A canvas (drawing panel) for drawing various types of shapes. You must implement at least one shape type at your own choice. This panel must be placed in the center part of the frame.
When the users execute mouse pressed operation, a shape must be drawn at the mouse location. You must use the properties defined in the configuration panel (at least one) and generate random values for others (color, etc.).
4. A control panel for managing the image being created. This panel will contain the buttons: Load, Save, Reset, Exit and it will be placed at the bottom part of the frame.
5. Use a file chooser in order to specify the file where the image will be saved (or load).


## Rezolvare Compulsory

1. I created a class MainFrame that extends JFrame. A MainFrame object contains a configuration panel, a drawing panel, and a control panel;
2. A ConfigurationPanel object has:  
   * A spinner(setNrSides) that allows you to choose the number of sides of the polygon you want to draw;  
   * A dropdown(setColor) that helps you choose if you want a random color or the green for your ploygon;
   * Two Labels describing what you need to select.
3. A DrawingPanel object has a frame, a weight and height, a buffered image and a graphics2d object. In this class i implemented
the mouse listener for drawing the shape at the mouse-clicked location.  
   The class RegularPolygon describes a regular polygon and creates the shape(the points) using the coordinates (x, y), the radius and the number of sides;
4. The class ControlPanel (extends JPanel). A ControlPanel object has a frame and 4 buttons: save, load, reset and exit;

![Drawing Polygons](my_polygons.png)



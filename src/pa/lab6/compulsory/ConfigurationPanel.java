package pa.lab6.compulsory;

import javax.swing.*;

public class ConfigurationPanel extends JPanel {
    final MainFrame frame;
    JLabel nrSides, colors;
    JSpinner setNrSides;
    JComboBox setColor;

    public ConfigurationPanel(MainFrame frame) {
        this.frame = frame;
        init();
    }

    /**
     * /**
     * This function initiate a ConfigurationPanel object. Instantiate two labels,
     * a spinner with the minimum value 3(a polygon has minimum 3 sides) and maximum 100,
     * a combo box (dropdown) with the choices Random and Green;
     * And adds them to the Panel
     */
    private void init() {
        nrSides = new JLabel("Number of sides:");
        setNrSides = new JSpinner(new SpinnerNumberModel(3, 3, 100, 1));
        setNrSides.setValue(3);
        colors = new JLabel("Color:");
        setColor = new JComboBox(new String[]{"Random","Green"});
        add(nrSides);
        add(setNrSides);
        add(colors);
        add(setColor);
    }
}

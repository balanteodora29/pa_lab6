package pa.lab6.compulsory;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ControlPanel extends JPanel{
    final MainFrame frame;
    JButton save = new JButton("Save");
    JButton load = new JButton("Load");
    JButton reset = new JButton("Reset");
    JButton exit = new JButton("Exit");

    public ControlPanel(MainFrame frame) {
        this.frame = frame;
        init();
    }

    /**
     * This function adds the buttons to the panel and adds the actionListeners.
     */
    private void init() {
        setLayout(new GridLayout(1, 4));
        add(save);
        add(load);
        add(reset);
        add(exit);
        save.addActionListener(this::save);
        load.addActionListener(this::load);
        reset.addActionListener(this::reset);
        exit.addActionListener(this::exit);
    }

    /**
     * The save option allows you to save your creation to the mentioned path.
     * @param e - the save button
     */
    private void save(ActionEvent e) {
        try {ImageIO.write(frame.canvas.image, "PNG", new File("E:/my_polygons.png"));
        } catch (IOException ex) { System.err.println(ex); }
    }

    /**
     * The load option allows you to draw over a previous drawing.
     * @param e - the load button
     */
    private void load(ActionEvent e){
        BufferedImage image = null;
        try{
            image = ImageIO.read(new File("E:/my_polygons.png"));
            this.frame.canvas.image = image;
            frame.canvas.graphics = frame.canvas.image.createGraphics();
            frame.canvas.repaint();
        }
        catch(IOException ex){
            System.err.println(ex);
        }
    }

    /**
     * The reset option clears the canvas.
     * @param e - the reset button
     */
    private void reset(ActionEvent e){
        this.frame.canvas.createOffscreenImage();
        this.frame.repaint();
    }

    /**
     * Clicking on exit button closes the window.
     * @param e
     */
    private void exit(ActionEvent e){
        System.exit(0);
    }
}


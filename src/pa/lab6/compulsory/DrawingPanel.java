package pa.lab6.compulsory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.Objects;
import java.util.Random;

public class DrawingPanel extends JPanel {
    final MainFrame frame;
    final static int W = 800, H = 600;
    BufferedImage image;
    Graphics2D graphics;

    public DrawingPanel(MainFrame frame) {
        this.frame = frame;
        createOffscreenImage();
        init();
    }

    /**
     * This function creates the background of the DrawingPanel, the place were the figures will be painted.
     */
    public void createOffscreenImage() {
        image = new BufferedImage(W, H, BufferedImage.TYPE_INT_ARGB);
        graphics = image.createGraphics();
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, W, H);
    }

    /**
     * This function initiate the DrawingPanel and adds a mouse pressed listener. A figure(in our case polygon) will be
     * draw at the location.
     */
    private void init() {
        setPreferredSize(new Dimension(W, H));
        setBorder(BorderFactory.createEtchedBorder());
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                drawShape(e.getX(), e.getY()); repaint();
            }
        });
    }

    /**
     * This function helps with drawing a shape at the location given by the x and y parameters;
     * We generate a random radius for the shape that will pe draw, and use the nr of sides selected by the user;
     * If the user selected the Green option for color, we will draw a green figure, else, we will generate a new color;
     */
    private void drawShape(int x, int y) {
        Random r = new Random();
        int radius = r.nextInt(30) ;
        int sides = Integer.parseInt(frame.configurationPanel.setNrSides.getValue().toString());
        Color color;
        if (Objects.equals(frame.configurationPanel.setColor.getSelectedItem(), "Green")){
            color = Color.GREEN;
        }
        else {
            color = new Color(r.nextInt(255),r.nextInt(255),r.nextInt(255),200);
        }
        graphics.setColor(color);
        graphics.fill(new RegularPolygon(x, y, radius, sides));
    }

    @Override
    public void update(Graphics g) { }

    @Override
    protected void paintComponent(Graphics g) {
        g.drawImage(image, 0, 0, this);
    }

}


package pa.lab6.compulsory;

import javax.swing.*;
import java.awt.*;

import static javax.swing.SwingConstants.CENTER;

public class MainFrame extends JFrame {
    ConfigurationPanel configurationPanel;
    DrawingPanel canvas;
    ControlPanel controlPanel;

    public MainFrame() {
        super("Draw polygons");
        init();
    }

    /**
     * This function initiate a MainFrame object. Sets the close operation,
     * instantiate a configuration panel, a canvas(drawing panel) and a control panel.
     */
    private void init() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        configurationPanel = new ConfigurationPanel(this);
        canvas = new DrawingPanel(this);
        controlPanel = new ControlPanel(this);
        add(canvas, CENTER);
        add(configurationPanel, BorderLayout.NORTH);
        add(controlPanel,BorderLayout.SOUTH);
        pack();
    }
}

